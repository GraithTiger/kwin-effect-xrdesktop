/********************************************************************
 KWin - the KDE window manager
 This file is part of the KDE project.

Copyright (C) 2006-2007 Rivo Laks <rivolaks@hot.ee>
Copyright (C) 2010, 2011 Martin Gräßlin <mgraesslin@kde.org>
Copyright (C) 2012 Philipp Knechtges <philipp-dev@knechtges.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "kwinconfig.h" // KWIN_HAVE_OPENGL

#include "kwinglplatform.h"
#include "kwinglutils.h"
#include "kwinglutils_funcs.h"

#include "kwingltexture_p.h"

#include <QImage>
#include <QMatrix4x4>
#include <QPixmap>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>

#include <kwineffects.h>

#include "kwingltexture.h"
#include "kwingltexture2.h"

namespace KWin {

GLTexturePrivate &GLTexture2::createSetPrivate()
{
    d_ptr = new GLTexturePrivate();
    return *d_ptr;
}

GLTexture2::GLTexture2(GLenum internalFormat, int width, int height, int levels, GLuint texture)
    : GLTexture(createSetPrivate())
{
    Q_D(GLTexture);

    d->m_target = GL_TEXTURE_2D;
    d->m_scale.setWidth(1.0 / width);
    d->m_scale.setHeight(1.0 / height);
    d->m_size = QSize(width, height);
    d->m_canUseMipmaps = levels > 1;
    d->m_mipLevels = levels;
    d->m_filter = levels > 1 ? GL_NEAREST_MIPMAP_LINEAR : GL_NEAREST;

    d->updateMatrix();

    d->m_internalFormat = internalFormat;
    d->m_texture = texture;
}

GLTexture2::~GLTexture2()
{
    delete d_ptr;
}

} // namespace KWin
